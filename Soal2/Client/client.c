#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>

#include <ctype.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void error(char *errmsg){
    perror(errmsg);
    exit(1);
}

bool remove_char(char* str, int index){
    if(index >= strlen(str)) return false;
    
    // printf("PTR OLD IS OLD _____%s_____\n", str);

    for(int i = 0; i < strlen(str) - index; i++){
        str[index + i] = str[index + i + 1];
        // printf("%c -> %c\n", str[index + i], str[index + i + 1]);
    }

    // printf("PTR NEW IS NEW _____%s_____\n\n", str);

    return true;
}

int main(int argc, char* argv[]){

    if(argc != 3){
        fprintf(stderr, "ERROR: Incorrect function parameter input.");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.");

    struct hostent* server = gethostbyname(argv[1]);
    if(!server){
        fprintf(stderr, "ERROR: Server unavailable.");
    }
    
    int port = atoi(argv[2]);

    struct sockaddr_in server_addr;
    bzero((char *) &server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    // bcopy((char*) server->h_addr, (struct sockaddr *) &server_addr.sin_addr.s_addr, server->h_length); 

    if(connect(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to connect.");

    int buffer_len = 256;
    char buffer_in[buffer_len];
    char buffer_out[buffer_len];
    int rw_flag;

    int logged_in = 0;

    printf("Initialized client on server %s with open port number %s\n", argv[1], argv[2]);

    int mode;
    int submode;

    char* token;

    while(1){
        //READ MODE & SUBMODE
        if(1){
            bzero(buffer_in, buffer_len);
            rw_flag = read(socket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read to file.");
            token = strtok(buffer_in, " ");
            mode = atoi(token);
            token = strtok(NULL, " ");
            submode = atoi(token);
            bzero(buffer_in, buffer_len);

            printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

        }

        if(!mode){

            //READ PROMPT
            bzero(buffer_in, buffer_len);
            rw_flag = read(socket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read to file.");
            printf("%s", buffer_in);

            //WRITE INPUT
            bzero(buffer_out, buffer_len);
            fgets(buffer_out, buffer_len, stdin);
            rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to read file.");
            printf("\n");
            
            if(atoi(buffer_out) == 3){
                printf("-----END PROGRAM-----\n");
                break;
            }
        }


        else if(mode == 1){
            //READ MODE & SUBMODE
            if(1){
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                token = strtok(buffer_in, " ");
                mode = atoi(token);
                token = strtok(NULL, " ");
                submode = atoi(token);
                bzero(buffer_in, buffer_len);

                // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

            }

            while(submode == 0){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);

                    // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

                }
            }

            bzero(buffer_in, buffer_len);

            while(submode == 1){
                if(!mode) break;

                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);

                    // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

                }
            }
        }


        else if(mode == 2){
            //READ MODE & SUBMODE
            if(1){
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                token = strtok(buffer_in, " ");
                mode = atoi(token);
                token = strtok(NULL, " ");
                submode = atoi(token);
                bzero(buffer_in, buffer_len);

                // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

            }

            // printf("-----REGISTERING MODE %d SUBMODE %d-----\n", mode, submode);

            while(submode == 0){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s ", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    // printf("MODE SUBMODE BUFFER %s", buffer_in);
                    bzero(buffer_in, buffer_len);

                    // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

                }
            }

            while(submode == 1){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s ", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    // printf("MODE SUBMODE BUFFER %s", buffer_in);
                    bzero(buffer_in, buffer_len);

                    // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

                }
            }

            // printf("\n\n\nREACHED TERMINAL POINT\n\n\n");
        }
    
        else if(mode == 3){

            while(1){
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);

                    // printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);

                }
                
                if(submode == 0){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");
                }

                else if(submode == 1){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");
                }
                
                else if(submode == 2){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    // printf("COUNT IS -----%s-----\n", buffer_in);

                    //CONFIRM READ STATUS
                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");

                    int count = atoi(buffer_in);

                    for(int i = 0; i < count; i++){
                        
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    }
                }
                
                else if(submode == 3){break;}
                
                else if(submode == 4){
                    char* ptr;
                    char copy[buffer_len];
                    ptr = strchr(buffer_out, ' ');
                    if(ptr == NULL) break;
                    strcpy(copy, ptr);
                    remove_char(copy, 0);
                    remove_char(copy, strlen(copy) - 1);

                    char problem_path[buffer_len];

                    strcpy(problem_path, "/home/nfpurnama/Downloads/");
                    strcat(problem_path, copy);
                    strcat(problem_path, "/");

                    // printf("PROBLEM REQUESTED IS IN-> -----%s-----\n", problem_path);

                    mkdir(problem_path, 0700);

                    char desc_path[buffer_len];
                    char input_path[buffer_len];

                    strcpy(desc_path, problem_path);
                    strcat(desc_path, "description.txt");

                    strcpy(input_path, problem_path);
                    strcat(input_path, "input.txt");
                    
                    int count;
                    FILE* desc_fp = fopen(desc_path, "w");
                    FILE* input_fp = fopen(input_path, "w");

                    // printf("BUFFER_IN BEFORE COUNT DESCRIPTION -> ******%s******\n", buffer_in);

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    // printf("COUNT BEFORE IS -----%s-----\n", buffer_in);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    // printf("COUNT BUFFER IS -----%s-----\n", buffer_in);

                    count = atoi(buffer_in);
                    // printf("COUNT DESCRIPTION -> -----%d-----\n", count);

                    //CONFIRM READ STATUS
                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    for(int i = 0; i < count; i++){
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        // printf("CURRENT LINE IS -> -----%s-----\n", buffer_in);
                        fprintf(desc_fp, "%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    }


                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    // printf("COUNT BEFORE IS -----%s-----\n", buffer_in);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    // printf("COUNT BUFFER IS -----%s-----\n", buffer_in);

                    count = atoi(buffer_in);
                    // printf("COUNT DESCRIPTION -> -----%d-----\n", count);

                    //CONFIRM READ STATUS
                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    for(int i = 0; i < count; i++){
                        // printf("CURRENT LINE NUMBER %d\n", i);
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        // printf("CURRENT LINE IS -> -----%s-----\n", buffer_in);
                        fprintf(input_fp, "%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    }

                    

                    fclose(desc_fp);
                    fclose(input_fp);
                }
                
                else if(submode == 5){
                    
                    // printf("COMMAND IN %s\n", buffer_out);
                    char* token;
                    char problem_title[buffer_len];
                    char output_path[buffer_len];

                    token = strtok(buffer_out, " ");
                    token = strtok(NULL, " ");
                    strcpy(problem_title, token);
                    token = strtok(NULL, " ");
                    strcpy(output_path, token);
                    remove_char(output_path, strlen(output_path) - 1);
                    // printf("COMMAND IN %s\nPROBLEM IN %s\nOUTPUT IN %s----\n", buffer_out, problem_title, output_path);

                    FILE *answer_fp = fopen(output_path, "r");
                    int count = 0;
                    while(fgets(buffer_in, buffer_len, answer_fp)) count++;
                    fclose(answer_fp);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d", count);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    // printf("%s", buffer_in);

                    if(atoi(buffer_in)){
                        answer_fp = fopen(output_path, "r");
                        for(int i = 0; i < count; i++){
                            fgets(buffer_out, buffer_len, answer_fp);
                            // printf("%d. %s\n", i, buffer_out);
                            
                            //WRITE INPUT
                            rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to read file.");
                            // printf("\n");
                            
                            while(strcmp(buffer_in, "FINISHED READING.")){
                                // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(socket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                            }
                            
                            
                            bzero(buffer_in, buffer_len);
                        }
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);

                    }else{
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);
                    }
                }
            }
        }
    }

    return 0;
}