#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <wait.h>
#include <fcntl.h>
#define PORT 8888


int main (int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char command[100];
	pid_t child_id;
	int fp;
	ssize_t read_stat;

	scanf("%[^\n]s", command);
	if(strcmp(command, "send hartakarun.zip") != 0) {
		printf("Invalid command, exiting program");
		return 0;
	}

	
	char buffer[1024];
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons (PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/angela/Client/hartakarun.zip", "/home/angela/shift3/hartakarun", NULL};
		if(execvp("/bin/zip", argv) == -1) {
			perror("zip failed!");
			exit(EXIT_FAILURE);
		}
	}
	else {
	while(wait(NULL)>0);
	fp = open ("hartakarun.zip", O_RDONLY);
	while (1) {
		read_stat = read(fp, buffer, 1024);
		if (read_stat == 0) break;
		if (read_stat == -1) {
			perror("Can't read file:");
			exit(1);
		}
		if (write(sock, buffer, read_stat) == -1) {
			perror("Can't write file:");
			exit(1);
		}
	}
}
	close(fp);
	return 0;
}
