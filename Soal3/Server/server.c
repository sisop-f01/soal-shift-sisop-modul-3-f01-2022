#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#define PORT 8888

int main(int argc, char const *argv[]) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[20000] = {0};
	int fp;
	char *message = "hartakarun.zip has been sent";
	ssize_t read_stat;

	if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if(listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}


	while(1) {
	if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}
		fp = open ("hartakarun.zip", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
		if (fp == -1) {
			perror ("Can't open file:");
			exit(1);
		}
		do {
			read_stat = read(new_socket, buffer, 1024);
			if (read_stat == -1) {
				perror ("Can't read file:");
				exit(1);
			}
			if (write(fp, buffer, read_stat) == -1) {
				perror("Can't write file:");
				exit(1);
			}
		}
		while (read_stat > 0);
		close (fp);
		close (new_socket);
		printf("hartakarun.zip has been sent\n");
	}
	return 0;
}
