# Soal Shift Sisop Modul 3 F01 2022



## Anggota Kelompok
1. Angela Oryza Prabowo
2. Azzura Ferliani Ramadhani
3. Naufal Adli Purnama
## Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

- `a.` Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
- `b.` Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
- `c.` Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
- `d.` Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
- `e.` Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

`Note:`
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

### Penyelesaian
+ `a.` Untuk menyelesaikan permasalahan, array perlu diinisialisasi untuk menampung thread
    ```c
        pthread_t tid[2];
    ```
    Selanjutnya akan dibuat fungsi `download_unzip` yang berguna untuk mendownload dan mengunzip file yang      telah didownload yang didalamnya terdapat deskripsi untuk link download di `*url[]`, file zip di `*fileName[]`, dan  nama folder di `*folder[]`
    ```c
    void* download_unzip(void *arg){
    unsigned long i=0;
    pthread_t id=pthread_self();
    int iter, status;

    char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		           "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
	
    char *fileName[] = {"/home/azzuraf/modul3/quote.zip","/home/azzuraf/modul3/music.zip"};

    char *folder[] = {"/home/azzuraf/modul3/quote","/home/azzuraf/modul3/music"};
    ```
    Di dalam fungsi tersebut juga terdapat dua thread yang berguna untuk membuat directory baru, mendownload file zip dari link gdrive, mengekstrak file, dan menghapus file. Thread pertama akan berjalan untuk keperluan array pertama yaitu file quote.
    ```c
    if(pthread_equal(id, tid[0])){
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[0], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[0], "-O", fileName[0], NULL};
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[0], "-d", "/home/azzuraf/modul3/quote", NULL};	
			execv("/usr/bin/unzip", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[0], NULL};
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
    }
    ```
    Selanjutnya ada thread kedua yang akan berjalan untuk keperluan array kedua yaitu file music.
    ```c
    else if(pthread_equal(id, tid[1])){
        if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[1], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[1], "-O", fileName[1], NULL};		
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[1], "-d", "/home/azzuraf/modul3/music", NULL};
			execv("/usr/bin/unzip", argv);
		}	
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[1], NULL};	
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
    }
    ```
<br>

+ `b.` Untuk menyelesaikan permasalahan kedua, diperlukan array untuk menginisialisasi thread
    ```c
    pthread_t tid1[2];
    ```
    Selanjutnya perlu dibuat set array character dari base64 seperti dibawah ini:
    ```c
    char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    ```
    Selanjutnya dibuat fungsi `base64_decode` yang berguna untuk mendecode file yang berada pada folder `music` maupun `quote`. Hal yang dilakukan pada fungsi ini adalah sebagai berikut:

    - mengalokasikan memori untuk plaintext dengan ukuran 3/4 dari encoded
    - melakukan _looping_ pada tiap character encoded sampai akhir (\0)
    - selama k < 64 dan indeks k pada map tidak sama dengan indeks i pada encoded, nilai k akan ditambah
    - simpan k ke dalam buffer selanjutnya

    ```c
    char *base64_decode(char *encoded)
    {
        char counts = 0;
        char buffer[4];
        char *decoded = malloc(strlen(encoded) * 3 / 4);
        int i = 0, p = 0;

        for (i = 0; encoded[i] != '\0'; i++)
        {
            char k;
            for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
                ;
            buffer[counts++] = k;
            if (counts == 4)
            {
                decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
                if (buffer[2] != 64)
                    decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
                if (buffer[3] != 64)
                    decoded[p++] = (buffer[2] << 6) + buffer[3];
                counts = 0;
            }
        }
        decoded[p] = '\0';
        return decoded;
    }
    ```
    Kemudian akan dibuat thread untuk mendecode `music` dan `quote`.
    ```c
    void *thread_decode(void *arg)
    {
        pthread_t id = pthread_self();
        char path1[50];
        char path2[50];
        char filename[50];
        if (pthread_equal(id, tid1[1]))
        {
            strcpy(path1, "/home/azzuraf/modul3/music/");
            strcpy(path2, "/home/azzuraf/modul3/music/%s");
            strcpy(filename, "/home/azzuraf/modul3/music/music.txt");
        }
        else
        {
            strcpy(path1, "/home/azzuraf/modul3/quote/");
            strcpy(path2, "/home/azzuraf/modul3/quote/%s");
            strcpy(filename, "/home/azzuraf/modul3/quote/quote.txt");
        }
        DIR *dir;
        struct dirent *dp;
        dir = opendir(path1);
        FILE *file, *txt;

        if (dir != NULL)
        {
            while ((dp = readdir(dir)))
            {
                char str[100] = "";
                char fullpath[1000] = "";
                if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
                {
                    sprintf(fullpath, path2, dp->d_name);
                    file = fopen(fullpath, "r");
                    while (fgets(str, 100, file) != NULL)
                    {
                        txt = fopen(filename, "a");
                        fprintf(txt, "%s\n", base64_decode(str));
                        fclose(txt);
                    }
                    fclose(file);
                }
            }
            (void)closedir(dir);
        }
        else
            perror("Cannot open directory");
        return NULL;
    }
    ```
<br>

+  `c.` Buat folder baru bernama `hasil`

    ```c
    if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};	
		execv("/usr/bin/mkdir", argv);
    }
    while(wait(&status) > 0);
    ```
    Lalu selanjutnya file `.txt` yang berada pada folder `quote` dan `music` dipindahkan ke folder `hasil`
    ```c
    if(fork() == 0){
        char *argv[] = {"mv", "/home/azzuraf/modul3/music/music.txt", "/home/azzuraf/modul3/hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "/home/azzuraf/modul3/quote/quote.txt", "/home/azzuraf/modul3/hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);
    ```
<br>
 
 + `d.` Melakukan zip folder hasil dengan menggunakan password **mihinomenestazzura**
    ```c
    if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestazzura", "-r", "hasil", "hasil", NULL}; 
		execv("/usr/bin/zip", argv);
    }while(wait(&status) > 0);
	
    for(int i = 0; i < 2; i++){	
        if(fork() == 0){
            char *argv[] = {"rm", "-rf", folder[i], NULL};
            execv("/usr/bin/rm", argv);
        }
    }
    ```
<br>

+ `e.` Dibuat fungsi `unzip` untuk mengunzip file `hasil.zip` karena akan dimasukkan file baru
    ```c
    void unzip(){
    pid_t child;
    child = fork();
    if(fork() == 0){
        char *unzip[5] = {"unzip", "-P", "mihinomenestazzura", "/home/azzuraf/modul3/hasil.zip", NULL};
        execv ("/usr/bin/unzip",unzip);
    }else {
        printf("close\n");
        return;
    }
    }
    ```
    Diperlukan dua thread yang akan diinisialisasikan pada array
    ```c
    pthread_t tid2[2];
    ``` 
    Dibuat fungsi `no_txt` yang didalamnya akan mengunzip dan juga memasukkan **"No"** pada file `no.txt` secara bersamaan dengan menggunakan thread
    ```c
    void* no_txt(void *arg){
        unsigned long i=0;
        pthread_t id=pthread_self();
        int iter;
        if(pthread_equal(id,tid2[0]))
        {
            unzip();
        }
        else if(pthread_equal(id,tid2[1]))
        {	
            char *filename = "/home/azzuraf/modul3/no.txt";
            FILE *fw = fopen(filename, "w");
            if (fw == NULL)
            {
                printf("Error opening the file %s", filename);
                return 0;
            }
            fprintf(fw, "No\n");
            fclose(fw);
        }
        return NULL;
    }
    ```
<br>

### Dokumentasi

![Contoh file yang menggunakan password](/Dokumentasi/Soal1/Contoh_password.png) <br>
*Contoh file yang menggunakan password* <br>

![Isi Music.txt](/Dokumentasi/Soal1/Isi_Music.png) <br>
*Isi Music.txt* <br>

![Isi Quote.txt](/Dokumentasi/Soal1/Isi_Quote.png) <br>
*Isi Quote.txt* <br>

![Isi No.txt](/Dokumentasi/Soal1/Isi_No.png) <br>
*Isi No.txt* <br>

### Kendala
Kesulitan pada bagian decode. Belum diketahui mengapa alokasi diharuskan 3/4 dari encode file.

## Soal 2

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

+ `a.` Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file __users.txt__ dengan format __username:password__. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di __users.txt__ yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

    <br>

    + Username unique (tidak boleh ada user yang memiliki username yang sama
    + Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
  
    <br>
    Format users.txt:

    ```
        username:password
        username2:password2
    ```

<br>


+ `b.` Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama __problems.tsv__ yang terdiri dari __judul problem dan author problem (berupa username dari author), yang dipisah dengan \t__. File otomatis dibuat saat server dijalankan.

<br>

+ `c.` __Client yang telah login__, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
    
    <br>
    
    + Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
    + Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
    + Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
    + Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

    <br>
    Contoh:
    
    Client-side

    ```
    add
    ```

    Server-side

    ```
    Judul problem:
    Filepath description.txt:
    Filepath input.txt:
    Filepath output.txt:
    ```

    Client-side

    ```
    <judul-problem-1>
    <Client/description.txt-1>
    <Client/input.txt-1>
    <Client/output.txt-1>
    ```


    Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file __problems.tsv__.


<br>

+ `d.` __Client yang telah login__, dapat memasukkan command `see` yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

    ```
    <judul-problem-1> by <author1>
    <judul-problem-2> by <author2>
    ```

<br>

+ `e.` __Client yang telah login__, dapat memasukkan command `download <judul-problem>` yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

<br>

+ `f.` __Client yang telah login__, dapat memasukkan command `submit <judul-problem> <path-file-output.txt>`.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

<br>

+ `g.` Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

    Note:
    + Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
    + Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
    + Untuk error handling jika tidak diminta di soal tidak perlu diberi.


<br>
<br>

## Penyelesaian

+ `a.` Server akan diinisialisasikan socketnya dengan menggunakan kode standar yaitu seperti berikut:

    ```C
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.\n");

    int port = atoi(argv[1]);

    struct sockaddr_in server_addr;
    bzero((char *) &server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);
    
    if(bind(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to bind socket.");
    listen(socket_fd, 5);
    ```
  
    Client akan dihubungkan ke pada socket server yang telah diinisialisasi dengan menggunakan fungsi `connect()` seperti berikut:

    ```c
    struct sockaddr_in client_addr;
    socklen_t  client_len = sizeof(client_addr);
    int newsocket_fd = accept(socket_fd, (struct sockaddr *) &client_addr, &client_len);
    if(newsocket_fd < 0) error("ERROR: Accept failed.\n");
    ```

    Selanjutnya, dibuatkan implementasi sistem pendaftaran akun baru sesuai ketentuan soal yang dilakukan melalui fungsi `check_user()` dan `check_pass()` untuk memastikan input user/password valid. Berikut source code kedua fungsi tersebut:

    + `check_user()` mengecek apakah ada inputan user sudah digunakan oleh user lain. Apabila tidak, akan dianggap valid.
    
        ```C
        bool check_user(char* buffer){
        FILE *fp = fopen("/home/nfpurnama/users.txt", "r");
        int len = 256;
        char temp[len];
        char copy[len];

        char* ptr;
        long long index;

        ptr = strchr(buffer, '\n');

        printf("\n\nPTR _____%s_____\nBUFFER _____%s_____\n", ptr, buffer);
        
        if(ptr != NULL){
            index = (long long) (ptr - buffer);
            strcpy(copy, buffer);
            bzero(buffer, len);
            strncpy(buffer, copy, index);
        }

        printf("BUFFER GOING IN _____%s_____\n", buffer);
        printf("SIZEOF BUFFER _____%lu_____\n", sizeof(buffer));

        while(fgets(temp, len, fp)) {
            if(temp == NULL) break;

            ptr = strchr(temp, ':');
            if(ptr != NULL){
                index = (int) (ptr - temp);
                strcpy(copy, temp);
                bzero(temp, len);
                strncpy(temp, copy, index);
            }

            printf("TEMP IS ------------%s-----------", temp);
            printf("%s strcmp %s == %d\n\n\n\n\n", buffer, temp, strcmp(buffer, temp));
            if(!strcmp(buffer, temp)) {
                fclose(fp);
                return false;
            }
        }

        fclose(fp);

        return true;
        }
        ```

    + `check_pass()` akan mengecek apakah inputan memiliki 6 karakter, minimal 1 huruf kapital, dan 1 angka.
  
        ```C
        bool check_pass(char* buffer){
        bool has_upper = false;
        bool has_lower = false;
        bool has_numeric = false;
        int char_count = 0;
        
        for(int i = 0; i < strlen(buffer); i++){
            if(isupper(buffer[i])){
                has_upper = true;
                char_count++;
            }
            if(islower(buffer[i])) {
                has_lower = true;
                char_count++;
            }if(isdigit(buffer[i])){
                has_numeric = true;
            }
        }

        return (has_upper && has_lower && has_numeric && (char_count >= 6));
        }
        ```

    Penyimpanan inputan user dan password yang valid pada file __users.txt__ dilakukan dengan cara berikut:

    ```C
    strcpy(buffer_out, strcat(strcat(user, ":"), pass));
                
    ptr = strchr(buffer_out, '\n');
    if(ptr != NULL){
        index = (int) (ptr - buffer_out);
        strcpy(copy, buffer_out);
        bzero(buffer_out, buffer_len);
        strncpy(buffer_out, copy, index);
    }

    // printf("FORMATTED USER:PASS IS %s----\n", buffer_out);

    FILE *fp = fopen("/home/nfpurnama/users.txt", "a");
    if(fp == NULL) error("ERROR: Failed to open users.txt file.");
    fprintf(fp, "%s\n", buffer_out);

    fclose(fp);
    ```

<br>

+ `b.` Inisialisasi file __problems.tsv__ dilakukan dengan cara berikut menggunakan fungsi `fopen()` pada mode append:

    ```C
    FILE *database_file;
    database_file = fopen("/home/nfpurnama/problems.tsv", "a+");
    if(database_file == NULL) {
        error("ERROR: FAILED TO OPEN DATABASE.\n");
    }
    ```

<br>

+ `c.` Praktikan memilih untuk membuat implementasi program yang menggunakan "mode" untuk menentukan fungsionalitas yang ingin dijalankan client. Beberapa mode memiliki beberapa "submode" yang digunakan untuk menentukan tahap dari pelaksanaan subprogram. Untuk client yang sudah berhasil login akan masuk mode 3 dan fungsi `add` merupakan submode 1. Seluruh subprogram `add` dibuat dalam suatu if statement seperti berikut:

    ```C
    else if(submode == 1){
    char problem_title[buffer_len];
    char desc_path[buffer_len];
    char input_path[buffer_len];
    char output_path[buffer_len];

    //WRITE PROMPT
    bzero(buffer_out, buffer_len);
    strcpy(buffer_out, ">>> ENTER PROBLEM TITLE: ");
    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
    if(rw_flag < 0) error("ERROR: Failed to write to file");

    //READ INPUT
    bzero(buffer_in, buffer_len);
    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
    if(rw_flag < 0) error("ERROR: Failed to read mode.");

    strcpy(problem_title, buffer_in);

    //WRITE PROMPT
    bzero(buffer_out, buffer_len);
    strcpy(buffer_out, ">>> ENTER DESCRIPTION FILE PATH: ");
    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
    if(rw_flag < 0) error("ERROR: Failed to write to file");

    //READ INPUT
    bzero(buffer_in, buffer_len);
    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
    if(rw_flag < 0) error("ERROR: Failed to read mode.");

    strcpy(desc_path, buffer_in);

    //WRITE PROMPT
    bzero(buffer_out, buffer_len);
    strcpy(buffer_out, ">>> ENTER INPUT FILE PATH: ");
    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
    if(rw_flag < 0) error("ERROR: Failed to write to file");

    //READ INPUT
    bzero(buffer_in, buffer_len);
    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
    if(rw_flag < 0) error("ERROR: Failed to read mode.");

    strcpy(input_path, buffer_in);

    //WRITE PROMPT
    bzero(buffer_out, buffer_len);
    strcpy(buffer_out, ">>> ENTER OUTPUT FILE PATH: ");
    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
    if(rw_flag < 0) error("ERROR: Failed to write to file");

    //READ INPUT
    bzero(buffer_in, buffer_len);
    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
    if(rw_flag < 0) error("ERROR: Failed to read mode.");

    strcpy(output_path, buffer_in);
    
    remove_char(problem_title, strlen(problem_title) - 1);
    remove_char(desc_path, strlen(desc_path) - 1);
    remove_char(input_path, strlen(input_path) - 1);
    remove_char(output_path, strlen(output_path) - 1);
    
    // printf("INPUTTED PATHS:\n%s\n%s\n%s\n%s\n%s\n", user, problem_title, desc_path, input_path, output_path);
    
    char dir_path[buffer_len];
    char home_path[buffer_len];
    strcpy(home_path, "/home/nfpurnama/");
    // printf("\nHOME PATH %s\n", home_path);
    strcpy(dir_path, strcat(home_path, problem_title));
    // printf("FOLDER TO BE CREATED %s\n\n", dir_path);
    mkdir(dir_path, 0700);

    bzero(buffer_out, buffer_len);
    strcpy(buffer_out, problem_title);
    strcat(buffer_out, "\t");
    strcat(buffer_out, user);
    strcat(buffer_out, "\n");
    // printf("TO BE APPENDED _____%s_____\n", buffer_out);

    FILE *database_file;
    database_file = fopen("/home/nfpurnama/problems.tsv", "a+");
    if(database_file == NULL) {
        error("ERROR: FAILED TO OPEN DATABASE.\n");
    }

    fprintf(database_file, "%s", buffer_out);

    fclose(database_file);

    char temp[buffer_len];

    bzero(temp, buffer_len);
    strcpy(temp,  dir_path);
    strcat(temp, "/description.txt");
    copy_file(temp, desc_path);

    bzero(temp, buffer_len);
    strcpy(temp, dir_path);
    strcat(temp, "/input.txt");
    copy_file(temp, input_path);

    bzero(temp, buffer_len);
    strcpy(temp, dir_path);
    strcat(temp, "/output.txt");
    copy_file(temp, output_path);

    submode = 0;
    }
    ```


<br>

+ `d.` Command `see` merupakan submode 2 dari mode 3 yang akan ditangani oleh subprogram berikut:

    ```C
    else if(submode == 2){
        FILE* problem_database = fopen("/home/nfpurnama/problems.tsv", "r");
        int count = 0;
        while(fgets(buffer_in, buffer_len, problem_database)) count++;
        fclose(problem_database);

        sprintf(buffer_out, "%d", count);
        printf("LINE COUNT OF -----%s------\n", buffer_out);

        //WRITE PROMPTp
        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
        if(rw_flag < 0) error("ERROR: Failed to write to file");     

        while(strcmp(buffer_in, "FINISHED READING.")){
            printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
            bzero(buffer_in, buffer_len);
            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read mode.");
        }
        
        bzero(buffer_in, buffer_len);

        problem_database = fopen("/home/nfpurnama/problems.tsv", "r");
        bzero(buffer_out, buffer_len);
        
        char* token;

        for(int i = 0; i < count; i++){
            fgets(buffer_out, buffer_len, problem_database);
            token = strtok(buffer_out, "\t");
            strcpy(copy, token);
            strcat(copy, " by ");
            token = strtok(NULL, "\t");
            strcat(copy, token);
            strcpy(buffer_out, copy);

            //WRITE PROMPT
            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
            bzero(buffer_out, buffer_len);

            printf("%s", buffer_out);
            
            while(strcmp(buffer_in, "FINISHED READING.")){
                printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");
            }
            
            bzero(buffer_in, buffer_len);
        }

        fclose(problem_database);
        // mode = 3;
        submode = 0;
    }
    ```

<br>

+ `e.` Command `download` merupakan submode 4 dari mode 3 yang akan ditangani oleh subprogram berikut:

    ```C
    else if(submode == 4){
        submode = 0;
        ptr = strchr(buffer_in, ' ');
        if(ptr == NULL) break;
        strcpy(copy, ptr);
        remove_char(copy, 0);

        char problem_path[buffer_len];

        strcpy(problem_path, "/home/nfpurnama/");
        strcat(problem_path, copy);
        strcat(problem_path, "/");

        // printf("PROBLEM REQUESTED IS IN-> -----%s-----\n", problem_path);

        char desc_path[buffer_len];
        char input_path[buffer_len];

        strcpy(desc_path, problem_path);
        strcat(desc_path, "description.txt");

        strcpy(input_path, problem_path);
        strcat(input_path, "input.txt");
        
        FILE* desc_fp = fopen(desc_path, "r");
        FILE* input_fp = fopen(input_path, "r");

        int count = 0;
        while(fgets(buffer_in, buffer_len, desc_fp)) count++;
        fclose(desc_fp);

        bzero(buffer_out, buffer_len);
        sprintf(buffer_out, "%d", count);
        // printf("LINE COUNT OF DESCRIPTION -----%s------\n", buffer_out);

        //WRITE PROMPT
        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
        if(rw_flag < 0) error("ERROR: Failed to write to file");                        
        bzero(buffer_out, buffer_len);
        
        while(strcmp(buffer_in, "FINISHED READING.")){
            // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
            bzero(buffer_in, buffer_len);
            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read mode.");
        }
        
        bzero(buffer_in, buffer_len);

        desc_fp = fopen(desc_path, "r");

        for(int i = 0; i < count; i++){
            fgets(buffer_out, buffer_len, desc_fp);
            // printf("CURRENT LINE DESCRIPTION -> -----%s-----\n", buffer_out);
            //WRITE PROMPT
            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
            bzero(buffer_out, buffer_len);

            while(strcmp(buffer_in, "FINISHED READING.")){
                // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");
            }
        
            bzero(buffer_in, buffer_len);
        }
        fclose(desc_fp);

        count = 0;
        while(fgets(buffer_in, buffer_len, input_fp)) count++;
        fclose(input_fp);

        bzero(buffer_out, buffer_len);
        sprintf(buffer_out, "%d", count);
        // printf("LINE COUNT OF DESCRIPTION -----%s------\n", buffer_out);

        //WRITE PROMPT
        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
        if(rw_flag < 0) error("ERROR: Failed to write to file");                        
        bzero(buffer_out, buffer_len);
        
        while(strcmp(buffer_in, "FINISHED READING.")){
            // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
            bzero(buffer_in, buffer_len);
            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read mode.");

        }
        
        bzero(buffer_in, buffer_len);

        input_fp = fopen(input_path, "r");

        for(int i = 0; i < count; i++){
            fgets(buffer_out, buffer_len, input_fp);
            // printf("CURRENT LINE DESCRIPTION -> -----%s-----\n", buffer_out);
            //WRITE PROMPT
            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
            bzero(buffer_out, buffer_len);

            while(strcmp(buffer_in, "FINISHED READING.")){
                // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");
            }

            bzero(buffer_in, buffer_len);
        }
        fclose(input_fp);
    }
    ```

<br>

+ `f.` Command `submit` merupakan submode 5 dari mode 3 yang akan ditangani oleh subprogram berikut:

    ```C
    else if(submode == 5){
        // printf("COMMAND IN %s\n", buffer_in);
        char* token;
        char problem_title[buffer_len];
        char output_path[buffer_len];

        token = strtok(buffer_in, " ");
        token = strtok(NULL, " ");
        strcpy(problem_title, token);
        token = strtok(NULL, " ");
        strcpy(output_path, token);

        // printf("COMMAND IN %s\nPROBLEM IN %s\nOUTPUT IN %s----\n", buffer_in, problem_title, output_path);
        
        char problem_path[buffer_len];
        strcpy(problem_path, "/home/nfpurnama/");
        strcat(problem_path, problem_title);
        strcat(problem_path, "/output.txt");
        FILE *answer_fp = fopen(problem_path, "r");

        int count = 0;
        while(fgets(buffer_in, buffer_len, answer_fp)) count++;
        fclose(answer_fp);

        //READ INPUT
        bzero(buffer_in, buffer_len);
        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
        if(rw_flag < 0) error("ERROR: Failed to read mode.");

        
        //WRITE PROMPT
        bzero(buffer_out, buffer_len);
        sprintf(buffer_out, "%d", (atoi(buffer_in) == count));
        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
        if(rw_flag < 0) error("ERROR: Failed to write to file");
        
        bool is_correct = true;

        answer_fp = fopen(problem_path, "r");
        if((atoi(buffer_in) == count)){
            char temp[buffer_len];
            for(int i = 0; i < count; i++){
                fgets(temp, buffer_len, answer_fp);

                //READ INPUT
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                // printf("%d. %s - %s**\n", i, temp, buffer_in);

                //CONFIRM READ STATUS
                bzero(buffer_out, buffer_len);
                strcpy(buffer_out, "FINISHED READING.");
                rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                
                // printf("%s", buffer_out);

                is_correct = is_correct & (!strcmp(buffer_in, temp));
                // printf("IS CORRECT -> --%d--", is_correct);
            }
            //WRITE PROMPT
            bzero(buffer_out, buffer_len);
            if(is_correct) strcpy(buffer_out, "\n\nAC\n\n");
            else strcpy(buffer_out, "\n\nWA\n\n");
            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to write to file");
        
        }else{
            //WRITE PROMPT
            bzero(buffer_out, buffer_len);
            strcpy(buffer_out, "\n\nWA\n\n");
            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to write to file");
        }
        submode = 0;
    }
    ```

<br>

+ `g.` Untuk memungkinkan antrian client digunakan sebuah infinite while loop yang setiap berakhir akan menutup socket client agar dapat menjadi koneksi socket baru ketika ada client yang ingin menyambung ke server. 

    ```C
    while(1){
        struct sockaddr_in client_addr;
        socklen_t  client_len = sizeof(client_addr);
        
        int newsocket_fd = accept(socket_fd, (struct sockaddr *) &client_addr, &client_len);
        if(newsocket_fd < 0) error("ERROR: Accept failed.\n");
        
        /*
        PROGRAM UNTUK MENANGANI CLIENT YANG TELAH MENYAMBUNG
            ..........
        PROGRAM SELESAI
        */

        close(newsocket_fd)
    }
    
    ```
  
<br>
<br>

### Dokumentasi

+ `a.` 
    + ![sebelum regiter](Dokumentasi/Soal2/a_before_register.png) 
    
    _sebelum register_
    + ![selama register](Dokumentasi/Soal2/a_during_register.png) 
    
    _selama register_
    + ![setelah register](Dokumentasi/Soal2/a_after_register.png) 
    
    _setelah register_
<br>

+ `c.`
    + ![sebelum add](Dokumentasi/Soal2/c_before_add.png)
    
        _sebelum add_
    + ![selama add](Dokumentasi/Soal2/c_during_add.png) 
    
        _selama add_
    + ![setelah add](Dokumentasi/Soal2/c_after_add.png) 
    
        _setelah add_
    + ![hasil add](Dokumentasi/Soal2/c_after_add2.png) 
    
        _hasil add_
<br>

+ `d.`
    + ![selama see](Dokumentasi/Soal2/d_see.png) 
    
        _selama see_
<br>

+ `e.`
    + ![sebelum download](Dokumentasi/Soal2/e_before_download.png) 
    
        _sebelum download_
    + ![selama download](Dokumentasi/Soal2/e_during_download.png) 
    
        _selama download_
    + ![setelah download](Dokumentasi/Soal2/e_after_download.png) 
    
        _setelah download_
    + ![hasil download](Dokumentasi/Soal2/e_after_download2.png) 
    
        _hasil download_
<br>

+ `f.`
    + ![selama submit](Dokumentasi/Soal2/f_submit.png) 
    
    _selama submit_
<br>
<br>

### Kendala

Kendala utama praktikan pada soal 2 merupakan proses debugging program. Hal ini diperburuk oleh keputusan desain program praktikan yang tidak mengandalkan fungsi sehingga mempersulit pencarian sumber bug. 

## Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

- `a.` Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder **“/home/[user]/shift3/”**. Kemudian working directory program akan berada pada folder **“/home/[user]/shift3/hartakarun/”**. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif <br>
- `b.` Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder **“Unknown”**. Jika file hidden, masuk folder **“Hidden”**.` <br>
- `c.` Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.<br>
- `d.` Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder **/home/[user]/shift3/hartakarun/”** akan di-zip terlebih dahulu dengan nama **“hartakarun.zip”** ke working directory dari program client.<br>
- `e.` Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server
```
send hartakarun.zip
```

`Note:`

-  Kategori folder tidak dibuat secara manual, harus melalui program C <br>
-  Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama. <br>
-  Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”) <br>
-  Dilarang juga menggunakan `fork`, `exec` dan `system()`, kecuali untuk bagian zip pada soal d

### Penyelesaian
Untuk menyelesaikan persoalan di atas, kita perlu memahami konsep terkait multithreading dan socket. Program pengkategorian akan menggunakan fungsi multithreading pada bahasa C untuk setiap file yang ingin diperiksa dan dikategorikan. Thread dalam C dapat dipanggil dan dibentuk melalui command berikut.
```c
int pthread_create(pthread_t *restrict tidp, 
					const pthread_attr_t *restrict attr, 
					void *(*start_rtn)(void *), 
					void *restrict arg);
```
dimana untuk fungsi yang dipanggil merupakan fungsi untuk melakukan *traverse* secara rekursif ke dalam directory yang ingin diperiksa **(/home/angela/shift3/hartakarun)**

Fungsi socket akan digunakan untuk menyelesaikan persoalan huruf d-e. Untuk mengirimkan file ke server, client perlu sebuah fungsi yang mampu menghubungkan portnya ke port server. Maka dari itu, digunakan socket sehingga dapat terjadi komunikasi antara server dengan client. Untuk membuat sebuah socket, digunakan outline program berikut.
- `Socket Client` <br>
```c
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
```
- `Socket Server` <br>
```c
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
```

1. `a` <br>
> - Sebelum melakukan ekstrasi file, folder shift3 harus terlebih dahulu dipastikan sudah terdapat dalam sistem operasi kita. Untuk membuat folder **shift3** dapat menggunakan command mkdir pada bash command line.
```bash
mkdir shift3
```
> - Kemudian untuk melakukan extract file **"hartakarun.zip"** dapat dilakukan secara manual dengan menggunakan bantuan bash pada command line di linux. Command untuk melakukan extract file dan memindahkannya ke directory yang diinginkan adalah sebagai berikut.
``` bash
unzip -q ~/Downloads/hartakarun.zip -d ~/shift3
```
> - Setelah itu, program pengkategorian berbahasa c harus ditelakkan ke dalam folder hasil ekstraksi **"hartakarun.zip."** Hal ini dapat dilakukan dengan menggunakan bantuan command `cp` atau `mv.` 
> - Untuk melakukan pengkategorian file berdasarkan ekstensinya, program akan melakukan *traverse* ke setiap sub folder hingga mendapatkan semua file yang terdapat dalam setiap sub-folder maupun yang di luar sub folder.
> - Apabila program menemukan sebuah file, maka program akan membentuk sebuah thread untuk melakukan pemeriksaan terkait ekstensi file tersebut, membuat folder sesuai dengan ekstensi file, dan memindahkan file tersebut ke folder terkait
> - Karena program yang dibuat diharapkan bersifat tidak *case sensitive*, maka setiap ekstensi file yang didapatkan harus diubah ke *lower case* dengan menggunakan fungsi `stringLwr.`
> - Apabila program menemukan sebuah sub-directory, maka program akan tetap melakukan traverse ke dalam isi sub-
directory tersebut. Namun setelah semua isi sub-directory tersebut dikunjungi, program akan menghapus sub-directory tersebut dan tidak akan mengkategorikannya.
``` c
void listFilesRecursively(char *basePath)
{
	char path_src[1000];
	struct dirent *dp;
	struct  stat statbuf;
	pthread_t t_id[200];
	int i=0;
	DIR *dir = opendir(basePath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0)
		{

			strcpy(path_src, basePath);
			strcat(path_src, "/");
			strcat(path_src, dp->d_name);

			listFilesRecursively(path_src);
			if (isDirectory(path_src))
			{
			rmdir(path_src);
			}
			else {
			pthread_create(&t_id[i], NULL, &auto_check, (void *)path_src);
			pthread_join(t_id[i], NULL);
			i++;
			}
		}
	}
	closedir(dir);
}

```

2. `b` <br>
> - Untuk memerika ekstensi sebuah file dapat digunakan fungsi `get_filename_ext`. Pada fungsi ini, program diperbolehkan untuk mendapatkan string setelah sebuah karakter tertentu dengan menggunakan fungsi bawaan dari C, yaitu `strrchr`. Dalam kasus ini, porgram akan mengembalikan string setelah tanda **'.'** terakhir. Hal ini dikarenakan string dengan titik terakhir pada file (.exe, .png) merupakan informasi ekstensi file. 
> - Untuk ekstensi file yang tidak diketahui, maka fungsi `strrchr` akan mengembalikan nilai NULL atau string yang sama dengan nama file karena tidak ditemukan tanda **.** Sehingga fungsi `get_filename_ext` akan mengembalikan string **Unknown**
> - Sedangkan untuk file yang hidden atau disembunyikan, file tersebut biasanya ditandai dengan tanda **.** di depan nama file. Maka dari itu, untuk file jenis ini, pemeriksaan yang digunakan adalah dengan menggunakan *array of char* dari nama file tersebut dengan merujuk pada indeks ke 0 (awal string). Apabila indeks ke 0 sebuah nama file sama dengan . maka fungsi `get_filename_ext` akan mengembalikan string **Hidden**
```c
int is_hidden(const char *name)
{
	return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}

char *get_filename_ext(char *filename) {
	struct stat st;
	char *dot = strrchr(filename, '.');
	if (is_hidden(filename)) return "Hidden";
	if(!dot || dot == filename) return "Unknown";
	else return dot+1;
}
```

3. `c`
> - Fungsi yang digunakan dalam thread untuk pengkategorian file adalah `auto_check` dengan path file sebelum dikategorikan sebagai parameternya. 
Di dalam fungsi `auto_check`, digunakan fungsi `basename` untuk memisahkan nama file dari pathnya sehingga dapat diperiksa ekstensi file tersebut dengan memanggil fungsi `get_filename_ext` seperti yang dijelaskan pada nomor **3b**.
> - Kemudian thread terkait akan membuat sebuah directory baru sesuai dengan ekstensi file tersebut.
> - Untuk memindahkan file tersebut ke directory yang sesuai dengan ekstensinya, dapat digunakan fungsi `rename` dengan mengubah nama path file asalnya ke path file sesuai dengan directory ekstensi terkait yang sebelumnya sudah dibuat.

```c
void *auto_check(void *args) {
char path_des[1000];
	char *path_src = (char*)malloc(sizeof(char));
	path_src = (char*)args;
	char *bname = basename(path_src);
	char *ext =strdup(get_filename_ext(bname));
	//printf("%s\n", bname);
	stringLwr(ext);
	if(strcmp(ext,"gz")== 0) {
		ext = "tar.gz";
	}
	strcpy(path_des, "/home/angela/shift3/hartakarun");
	strcat(path_des, "/");
	strcat(path_des, ext);
	strcat(path_des, "/");
	safe_create_dir(path_des);
	strcat(path_des, bname);
	rename(path_src, path_des);
}
```

4. `d`
> - Untuk melakukan zip folder hartakarun, maka dapat digunakan fungsi `execv()` dengan terlebih dahulu memanggil sebuah proses baru. Proses baru dapat dipanggil dengan menggunakan bantuan fungsi `fork()`. 
> - Agar file tersebut dapat disimpan ke working directory dari client, maka sebuah directory baru bernama **Client** akan dibuat dengan bantuan `mkdir` dan argumen untuk fungsi `execv()` akan dituliskan sebagai berikut.
```bash
	child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/angela/Client/hartakarun.zip", "/home/angela/shift3/hartakarun", NULL};
		if(execvp("/bin/zip", argv) == -1) {
			perror("zip failed!");
			exit(EXIT_FAILURE);
		}
	}

```

5. `e`
> - Untuk mengirimkan file ke server, kita perlu terlebih dahulu membuat socket agar port server dengan port client terhubung dengan menggunakan outline program yang sudah disebutkan pada abstrak soal sebelumnya.
> - Dalam program client, untuk mengirimkan file **hartakarun.zip**, client harus terlebih dahulu menuliskan command `"send hartakarun.zip"`. Apabila client memasukan string atau command lain, program akan keluar dengan menampilkan pernyataan bahwa command tidak sesuai.
```c
	scanf("%[^\n]s", command);
	if(strcmp(command, "send hartakarun.zip") != 0) {
		printf("Invalid command, exiting program");
		return 0;
	}
```
> - Setelah menuliskan command `"send hartakarun.zip"`, prpgram client akan melakukan *looping* untuk membaca keseluruhan file **hartakarun.zip** dan menuliskannya ke socket yang menghubungkannya dengan server.
```c
	while (1) {
		read_stat = read(fp, buffer, 1024);
		if (read_stat == 0) break;
		if (read_stat == -1) {
			perror("Can't read file:");
			exit(1);
		}
		if (write(sock, buffer, read_stat) == -1) {
			perror("Can't write file:");
			exit(1);
		}
	}

```
> - Pada program server, server akan menerima dan membaca setiap file yang dikirimkan oleh client dengan menggunakan *looping*. File-file tersebut kemudian akan dituliskan kembali ke dalam working directory dari server
```c
	while(1) {
	if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}
		fp = open ("hartakarun.zip", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
		if (fp == -1) {
			perror ("Can't open file:");
			exit(1);
		}
		do {
			read_stat = read(new_socket, buffer, 1024);
			if (read_stat == -1) {
				perror ("Can't read file:");
				exit(1);
			}
			if (write(fp, buffer, read_stat) == -1) {
				perror("Can't write file:");
				exit(1);
			}
		}
		while (read_stat > 0);
		close (fp);
		close (new_socket);
		printf("hartakarun.zip has been sent\n");
	}

```
### Dokumentasi

![Folder hartakarun setelah pengkategorian](/Dokumentasi/Soal3/hasil_kategori.png) <br>
*Folder hartakarun setelah pengkategorian* <br>

![Contoh isi dari folder kategori (jpg dan png)](/Dokumentasi/Soal3/contoh_isi.jpg) <br>
*Contoh isi dari folder kategori (jpg dan png)* <br>

![Working directory dari server dan client](/Dokumentasi/Soal3/working_dir.jpg) <br>
*Working directory dari server dan client* <br>

![Hasil unzip hartakarun.zip pada server dan client](/Dokumentasi/Soal3/hasil_zip.jpg) <br>
*Hasil unzip hartakarun.zip pada server dan client* <br>

### Kendala
Pada saat praktikum, file **hartakarun.zip** yang diterima oleh server tidak bisa diunzip. Hal ini dikarenakan program awalnya menggunakan fungsi `send()` dan `recv()` tanpa melakukan *looping* untuk setiap data yang terdapat pada file terkait. File *hartakarun.zip* yang seharusnya merupakan binary file dan memuat banyak file di dalamnya, justru malah dianggap sebagai sebuah file text biasa
